# vm-builder

We use `Dockerfiles` to build custom host and guest VMs based on Docker Alpine image. Follow this guide to [install Docker](https://docs.docker.com/engine/install/) if necessary.

Usage:

```bash
Build rootfs and initrd for a guest and host vm.

Usage: ./create.sh [option] [arguments] [subcommands]

Example:
          ./create.sh all --guest-file Dockerfile.guest --host-file Dockerfile.host
          ./create.sh guest --guest-file Dockerfile.guest
          ./create.sh host --host-file Dockerfile.host
          ./create.sh host
          ./create.sh guest

Options:
  -h,  --help          Display help

Arguments:
  guest                Build rootfs and initrd for guest VM.
  host                 Build rootfs and initrd for host VM.
  all                  Build rootfs and initrd for host and guest VM.
```

```bash
    $ ./create.sh guest -h


Build rootfs and initrd for a guest and host vm using a personalized Dockerfile.

Usage: ./create.sh [guest/host/all] [options]

Example:
          ./create.sh guest --guest-file Dockerfile.guest
          ./create.sh host --host-file Dockerfile.host
          ./create.sh all --guest-file Dockerfile.guest --host-file Dockerfile.host

Options:
  -h,  --help          Display help
  --guest-file string  Guest Dockerfile path (Default 'Dockerfile.guest')
  --host-file string   Host Dockerfile path (Default 'Dockerfile.host')

```

[Example](./example/) folder contains a test guest and host initrd to use.