#!/bin/bash
 set -e
#build rootfs and initrd for icecap based on alpine
distro="alpine"
timestamp=$(date +%F%H%M%S)

function create {
    #create guest $res $1
    image_name="${distro}-icecap-$1"
    rootfs_dir="${distro}-icecap-$1-rootfs"
    container_name="${image_name}-${timestamp}"
    
    docker build --platform linux/arm64 -t "${image_name}" -f $2 .
    docker run --platform linux/arm64 -d --name "${container_name}" --rm "${image_name}"
    docker export ${container_name} -o ${container_name}.tar
    mkdir ${rootfs_dir}
    tar -C ${rootfs_dir} -xf ${container_name}.tar
    rm ${container_name}.tar
    cp init-$1 ${rootfs_dir}/init
    cp cgroup-mount.sh ${rootfs_dir}/cgroup-mount.sh
    sudo cp init-$1 ${rootfs_dir}/sbin/init
    cd ${rootfs_dir}
    if [ $1 = "host" ]; then
    #--platform="linux/arm64"
        sudo ctr i pull --all-platforms docker.io/library/hello-world:latest
        sudo ctr i export --all-platforms hello-world.tar docker.io/library/hello-world:latest
        sudo chown $USER:$USER hello-world.tar 

        sudo ctr i pull --all-platforms docker.io/library/busybox:latest
        sudo ctr i export --all-platforms busybox.tar docker.io/library/busybox:latest
        sudo chown $USER:$USER busybox.tar 
        
        cp ../bin/* .
        if [ $3 = "initrd" ]; then
            find . -print0 | cpio --null -ov --format=newc | gzip -9 > ../${image_name}-initrd.cpio.gz
            cd ..
            rm -rf ${rootfs_dir}
        elif [ $3 = "block" ]; then
            cd ..
            sudo virt-make-fs --format=qcow2 --type=ext2 ${rootfs_dir} temp.ext2.qcow2
            qemu-img convert -f qcow2 temp.ext2.qcow2 -O qcow2 -o compat=0.10 $1.ext2.qcow2
            sudo rm temp.ext2.qcow2
            rm -rf ${rootfs_dir}
            qemu-img resize $1.ext2.qcow2 +10G
        fi
    else
        find . -print0 | cpio --null -ov --format=newc | gzip -9 > ../${image_name}-initrd.cpio.gz
        cd ..
        rm -rf ${rootfs_dir}
    fi
}

function usage { 
	echo "Build rootfs, initrd and a Qcow2 image (to be used as a block device) for a guest and host vm." 
    echo -e "\nUsage: $0 [format] [option] [arguments] [subcommands]"
    echo -e "\nExample:"
    echo "          ./create.sh block all --guest-file Dockerfile.guest --host-file Dockerfile.host"
    echo "          ./create.sh dir guest --guest-file Dockerfile.guest"
    echo "          ./create.sh initrd host --host-file Dockerfile.host"
    echo "          ./create.sh block host"
    echo "          ./create.sh initrd guest"
    echo -e "\nFormats:"
    echo -e "  dir                    Produce a folder that container root filesystem." 
    echo -e "  initrd                 Produce the root fs in a cpio compressed file."
    echo -e "  block                  Produce the root fs in a Qcow2 file."
    echo -e "\nOptions:"
    echo "  -h,  --help          Display help"
    echo -e "\nArguments:"
	echo -e "  guest                Build rootfs and initrd for guest VM." 
    echo -e "  host                 Build rootfs and initrd for host VM."
    echo -e "  all                  Build rootfs and initrd for host and guest VM."
    exit 0
}

function subcommand_usage {
    echo "Build rootfs, initrd and a Qcow2 image (to be used as a block device) for a guest and host vm using a personalized Dockerfile." 
    echo -e "\nUsage: $0 $1 [options]"
    echo -e "\nExample:"
    echo "          ./create.sh dir guest --guest-file Dockerfile.guest"
    echo "          ./create.sh initrd host --host-file Dockerfile.host"
    echo "          ./create.sh block all --guest-file Dockerfile.guest --host-file Dockerfile.host"
    echo -e "\nOptions:"
    echo "  -h,  --help          Display help"
    echo "  --guest-file string  Guest Dockerfile path (Default 'Dockerfile.guest')"
    echo "  --host-file string   Host Dockerfile path (Default 'Dockerfile.host')"
    exit 0
}

function get_file {
    declare -n ret="res"
    cnt=0; for arg in "${ARGS[@]}"; do
        [[ $arg == "--$1-file" ]] &&  ((cnt=cnt+1)) && break
        ((++cnt))
    done

    ret=${ARGS[cnt]}
    ret="${ret:-Dockerfile.$1}"
}

ARGS=( "$@" )
if ! [[ $# == 2 || $# == 3 || $# == 4 || $# == 6 ]];
then
    echo "Invalid argument."
	usage
fi 
if [[ ( $1 == "--help") ||  $1 == "-h" ]] 
then
	usage
fi 

if [[ $2 == "host" ]] || [[ $2 == "guest" ]] || [[ $2 = "all" ]]; then
    if [[ ( $3 == "--help") ||  $3 == "-h" ]] 
    then
        subcommand_usage
    fi 
    if [ $2 = "all" ]; then
        get_file "host"
        create host $res $1
        get_file "guest"
        create guest $res $1
    else
        get_file $2
        create $2 $res $1
    fi
else
    echo "Invalid argument."
    usage
fi

